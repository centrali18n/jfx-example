package com.zenjava.examples.hellojfxmaven;

import javafx.beans.InvalidationListener;
import javafx.beans.property.StringProperty;
import javafx.beans.Observable;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Button;
import net.spottog.i18n.Property.I18nProp;
import net.spottog.i18n.global.I18n;

import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.beans.property.StringProperty;
public class HelloController
{
    private static final Logger log = LoggerFactory.getLogger(HelloController.class);

    @FXML private TextField firstNameField;
    @FXML private TextField lastNameField;
    @FXML private Label messageLabel;
    //Das Label für den Vornamen
    @FXML private Label firstnamelabel;
    //Das Label für den Nachnamen
    @FXML private Label lastnamelabel;
    //Der Button um den Text Anzuzeigen
    @FXML private Button btnGo;
    //Konstruktor.
    public HelloController() {
    	//Das Label für den Vornamen Beobachtet das I18nProp
    	new I18nProp("Last Name").addListener(new InvalidationListener() {
			public void invalidated(Observable w) {
				lastnamelabel.setText(((StringProperty)w).get());
			}
		});
    	//Das Label für den Nachnamen Beobachtet das I18nProp
    	new I18nProp("First Name").addListener(new InvalidationListener() {
			public void invalidated(Observable w) {
				firstnamelabel.setText(((StringProperty)w).get());
			}
		});
    	//Der Button Beobachtet das I18nProp
    	new I18nProp("Say Hello").addListener(new InvalidationListener() {
			public void invalidated(Observable w) {
				btnGo.setText(((StringProperty)w).get());
			}
		});
    }
    public void sayHello() {

        String firstName = firstNameField.getText();
        String lastName = lastNameField.getText();

        StringBuilder builder = new StringBuilder();

        if (!StringUtils.isEmpty(firstName)) {
            builder.append(firstName);
        }

        if (!StringUtils.isEmpty(lastName)) {
            if (builder.length() > 0) {
                builder.append(" ");
            }
            builder.append(lastName);
        }

        if (builder.length() > 0) {
            String name = builder.toString();
            log.debug("Saying hello to " + name);
            messageLabel.setText(new I18n("Hello ").toString() + name);
        } else {
            log.debug("Neither first name nor last name was set, saying hello to anonymous person");
            messageLabel.setText(new I18n("Hello mysterious person").toString());
        }
    }
    public void SelectLanguageDeutsch() {
    	SelectLanguage("de");
    }
    public void SelectLanguageEnglisch() {
    	SelectLanguage("en");
    }
    public void SelectLanguage(String neueSprache){
    	Locale.setDefault(new Locale(neueSprache));
    	I18nProp.FireChangeLangugage();
    }

}
